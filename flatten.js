
function flatten(items)
{
    let flatArray=[]
    for(i=0;i<items.length;i++)
    {
        if(Array.isArray(items[i])==true)
        {
            flatArray=flatArray.concat(flatten(items[i])) //returns the function with new array which is nested array 
        }
        else{
            flatArray.push(items[i])
        }
    }
    return flatArray
}


module.exports= flatten
