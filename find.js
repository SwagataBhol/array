function find(items,cb)
{
    for (let i=0;i<items.length;i++)
    {
        
        if (cb(items[i],i)==true)
        {
            return items[i];
        }
    }
    return undefined ;
}
module.exports=find
